const container = document.getElementById('container');

Rx.Observable
  .timer(0, 5000)
  .take(10)
  .map(x => Math.floor(Math.random() * 5) + 1)
  .flatMap(x => {
    return Rx.Observable
      .ajax({ 
        url: 'https://baconipsum.com/api/?type=all-meat&paras=${ x }&format=html', 
        crossDomain: true, 
        responseType: 'text',
      });
  })
  .subscribe(x => {
    container.innerHTML = x.response;
  })

  