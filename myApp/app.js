

console.log('starting app')

const testme = () => {console.log('yes es6')}
testme();
//https://itnext.io/sharing-state-between-angularjs-and-angular-v6-with-redux-75e3abe7f4f3
//https://itnext.io/sharing-state-between-angularjs-and-angular-v6-with-redux-75e3abe7f4f3
// common module
angular.module('myApp.common', [])
.service('catService', function ($q) {
    this.meow = function () {
        console.log('meow')
    }

    this.getData = function () {
        var deferred = $q.defer()
        setTimeout(function () {
            console.log('resolve defer')
            deferred.resolve('resolved')
        }, 2000)
        return deferred.promise
    }
})
.factory('myCache', function($cacheFactory){
    return $cacheFactory('myData');
})

angular.module('myApp.child', ['myApp.common'])

// https://juristr.com/blog/2015/01/learning-ng-directives-access-scope-controller/
var tmp = angular.module('myApp.child')
    .directive('myDirective', function () {
        return {
            restrict: 'A',
            transclude: true,
            template: "<div style='background-color:red'><p>cached data {{cachedName}}</p><button ng-click=\"eat()\">click</button><p ng-repeat='item in items'>{{item.name}}{{item.disabled}}</p></div>",

            link: function (scope, element, attr) {
                scope.eat = function () {
                    console.log('eating')
                    scope.$emit('food.eaten')
                }
            },
            controller: function ($scope, myCache) {
                console.log('directive controller');
                
                var cachedData = myCache.get('data');
                $scope.cachedName = (cachedData) ? cachedData : 'no cache';
                $scope.init = function () {
                    $scope.items = [
                        {
                            name: 'a',
                            disabled: false,
                        },
                        {
                            name: 'b',
                            disabled: false,
                        }
                    ]
                }
                $scope.init();

                $scope.$on('initMenu', function(e, type){
                    console.log('received init menu request');
                    $scope.init();
                })
                
                $scope.items.forEach(function (item, index) {
                    console.log('foreach index' + index);
                    if (index == 1) {
                        $scope.items[index].disabled = true;
                    }
                })
            }
        }
    })
    .value('fixedID', 'Some fixed ID')

console.log('tmp', tmp)


angular.module('myApp', ['myApp.dog', 'myApp.child', 'myApp.common', 'myApp.billingSummary', 'ui.router'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.hashPrefix('')
        $urlRouterProvider.otherwise('/home')

        $stateProvider
            .state('home', {
                name: 'home',
                url: '/home',
                templateUrl: 'partial-index.html',
                controller: 'myController'
            })
            .state('about', {
                name: 'about',
                url: '/about',
                templateUrl: 'partial-about.html',
                controller: 'aboutController'
            })
    })

angular
    .module('myApp')
    .controller('myController', function myController($scope, $rootScope, myCache, catService, fixedID) {
        console.log('in myController')
        var _cache_data = myCache.get('data')
               
        myCache.put('data','abc');
        
        $scope.data = {
            name: 'my name',
            fixedID: fixedID,
            int: 1,
            cachedName: myCache.get('data'),
            input: 'say something',
            dogSize: 'big'
        }
        $scope.$watch('data.input', function(newVal, oldVal){
            console.log('changed');
        })
        // catService.meow();
        // catService.getData().then(function(data){console.log('resolved'+data)})

        var nbDigest = 0

        $scope.runMe = function () {
            console.log('runme')
            // $scope.data.int++;
        }
        $scope.$on('food.eaten', function (e, type) {
            console.log('i know food is eaten')
        })
        $rootScope.$watch(function () {
           // console.log('nbDigest' + nbDigest)
            nbDigest++
        })
    })
    .controller('aboutController', function aboutController($scope){
        $scope.$emit('initMenu')
    })
