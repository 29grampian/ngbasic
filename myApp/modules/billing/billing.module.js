angular.module('myApp.billingSummary', ['ui.router'])
	
	.config(function($stateProvider){
        console.log('myApp.billingSummary loaded');
		$stateProvider.state('billingSummary', {
            url: '/billingsummary',
            
            controller: 'billingSummaryCtrl',
            templateUrl: 'modules/billing/billing.summary.html'
                
           
        });
	});