angular.module('myApp.dog',[])
.directive('myDog', function(){
	return {
		restrict: 'E',
		scope:{
			dogName: '@',
			rating: '@',
			size: '@',
			bark: '@',
		},
		templateUrl: 'modules/dog/dog.directive.html',
		link: function(scope){
			scope._local = 'xxx'
		}
	}
})