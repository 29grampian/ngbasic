'use strict';


describe('dog module', function () {
	var injector, element, scope, element, isoscope;
	beforeEach(module('templates', 'myApp.dog'));

	// beforeEach(function(){
	// 	//https://coderwall.com/p/l4fvmq/injecting-custom-services-in-an-angularjs-unit-test
	// 	//https://thecodebarbarian.com/2015/06/12/testing-angularjs-directives
	// 	injector = angular.injector(['myApp']);
	// 	scope = $rootScope.

	// })
	beforeEach(inject(function($rootScope, $compile){

		element = angular.element(
			'<my-dog dog-name=\'abc\' size=\'big\' bark=\'woof\'></div>'
		);
		
		scope = $rootScope.$new();
		scope.data = {
			size: 'my name'
		}
		$compile(element)(scope);
		scope.$digest();

		isoscope = element.isolateScope();
		isoscope.name2 = 'bbb'
	}))

	it('is working', function(){
		console.log('testing dog')
		// console.log(element.html());
		// console.log(element.prop('innerHTML'));
		console.log( element.find('p.title').text() );
		
		expect(isoscope.dogName).toEqual('abc')

		expect(true).toBeTruthy()
	});
})