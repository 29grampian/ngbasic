var express = require('express')
var opn = require('opn')

var app = express()
app.use(express.static('myApp')) // myApp will be the same folder name.
app.get('/', function (req, res, next) {
  res.redirect('/')
})
app.listen(9000, 'localhost')
console.log('MyProject Server is Listening on port 9000')
opn('http://localhost:9000', { app: 'google-chrome' })
